package com.example.kriskelner.okcupid_test.model;


/**
 * Created by kris.kelner on 1/30/18.
 */

public class PhotoPath {
    public String large;
    public String small;
    public String medium;
    public String original;
    public String desktop_match;
}

/*
"large": "https://k3.okccdn.com/php/load_okc_image.php/images/0x0/640x560/36x36/684x684/0/15743311334557165678.jpg",
          "small": "https://k2.okccdn.com/php/load_okc_image.php/images/0x0/60x60/36x36/684x684/0/15743311334557165678.jpg",
          "medium": "https://k2.okccdn.com/php/load_okc_image.php/images/0x0/120x120/36x36/684x684/0/15743311334557165678.jpg",
          "original": "https://k2.okccdn.com/php/load_okc_image.php/images/36x36/684x684/0/15743311334557165678.jpg"
          "desktop_match": "https://k0.okccdn.com/php/load_okc_image.php/images/400x400/400x400/36x36/684x684/2/15743311334557165678.jpg",
* */