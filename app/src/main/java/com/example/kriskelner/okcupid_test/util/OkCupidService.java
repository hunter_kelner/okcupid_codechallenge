package com.example.kriskelner.okcupid_test.util;

import com.example.kriskelner.okcupid_test.model.OkCupidData;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by kris.kelner on 1/30/18.
 */

public interface OkCupidService {
    @GET("matchSample.json")
    Observable<OkCupidData> getUsers();
}
