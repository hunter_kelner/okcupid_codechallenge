package com.example.kriskelner.okcupid_test.presenter;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.util.Log;
import android.view.View;

import com.example.kriskelner.okcupid_test.model.OkCupidData;
import com.example.kriskelner.okcupid_test.model.OkCupidUser;
import com.example.kriskelner.okcupid_test.model.ViewModel;
import com.example.kriskelner.okcupid_test.util.OkCupidService;
import com.example.kriskelner.okcupid_test.util.ServiceApi;
import com.example.kriskelner.okcupid_test.view.MainActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by kris.kelner on 1/30/18.
 */

public class MainPresenter implements LifecycleObserver {

    private OkCupidData data;
    private OkCupidData matchesData;

    private BehaviorSubject<ViewModel> viewModelSubject = BehaviorSubject.create();

    public MainPresenter(MainActivity view){
        view.getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume(){
        loadUsers();
    }

    public Disposable getViewModel(Consumer<ViewModel> consumer){
        return viewModelSubject
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer);
    }

    public void displayMatches(){
        ViewModel model = new ViewModel();
        model.data = matchesData;
        viewModelSubject.onNext(model);
    }

    public void displayAllUsers(){
        ViewModel model = new ViewModel();
        model.data = data;
        viewModelSubject.onNext(model);
    }


    private void filterData(OkCupidData data){
        matchesData = new OkCupidData();
        matchesData.total_matches = data.total_matches;
        List<OkCupidUser> newList = new ArrayList<>();
        for(OkCupidUser user: data.data){
            if(user.liked){
                newList.add(user);
            }
        }
        matchesData.data = newList;
    }

    public void toggleLiked(OkCupidUser user){
        if(!matchesData.data.contains(user)){
            matchesData.data.add(user);
        }else{
            matchesData.data.remove(user);
        }

        Collections.sort(matchesData.data);
        if(matchesData.data.size()>6){
            matchesData.data = matchesData.data.subList(0,6);
        }

    }


    private void loadUsers(){
        ServiceApi api = new ServiceApi();
        OkCupidService service = api.getApi();
        service.getUsers()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OkCupidData>() {
                    @Override
                    public void accept(OkCupidData response) throws Exception {
                        ViewModel model = new ViewModel();
                        model.data = response;
                        data = response;
                        viewModelSubject.onNext(model);
                        filterData(data);
                    }
                });
    }
}
