package com.example.kriskelner.okcupid_test.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.kriskelner.okcupid_test.R;
import com.example.kriskelner.okcupid_test.model.OkCupidUser;
import com.example.kriskelner.okcupid_test.util.OnItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kris.kelner on 1/30/18.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private List<OkCupidUser> data;
    private final OnItemClickListener listener;


    private Context ctx;

    public DataAdapter(Context ctx, List<OkCupidUser> data, OnItemClickListener listener){
        this.listener = listener;
        this.ctx = ctx;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.user_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OkCupidUser user = data.get(position);
        holder.tvName.setText(user.username);
        holder.tvInfo.setText(String.format(ctx.getString(R.string.user_info), user.age, user.city_name, user.state_code));
        holder.tvMatch.setText(String.format(ctx.getString(R.string.user_match), user.match/100));

        Glide.with(ctx).load(user.photo.full_paths.large) .apply(new RequestOptions()
                .centerCrop() // keep memory usage low by fitting into (w x h) [optional]
        ).into(holder.iView);
        holder.cView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.llCard.setBackgroundResource(user.liked?android.R.color.white:R.color.liked);
                listener.onItemClick(user);
            }
        });
        holder.llCard.setBackgroundResource(user.liked?R.color.liked:android.R.color.white);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void changeDataSet(List<OkCupidUser> data){
        this.data = data;
        this.notifyDataSetChanged();
    }




    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view)
        CardView cView;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_info)
        TextView tvInfo;
        @BindView(R.id.tv_match)
        TextView tvMatch;
        @BindView(R.id.imageView)
        ImageView iView;
        @BindView(R.id.ll_card)
        LinearLayout llCard;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
