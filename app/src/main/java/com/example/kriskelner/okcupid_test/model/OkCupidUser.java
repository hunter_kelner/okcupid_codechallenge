package com.example.kriskelner.okcupid_test.model;


import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Created by kris.kelner on 1/30/18.
 */
public class OkCupidUser implements Comparable<OkCupidUser> {
    public int match;
    public boolean liked;
    public OkCupidPhoto photo;
    public String state_code;
    public String age;
    public String city_name;
    public String username;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OkCupidUser that = (OkCupidUser) o;

        return TextUtils.equals(state_code, that.state_code) &&
                TextUtils.equals(age, that.age) &&
                TextUtils.equals(city_name, that.city_name) &&
                TextUtils.equals(username, that.username);

    }


    @Override
    public int compareTo(@NonNull OkCupidUser okCupidUser) {
        return Integer.compare(okCupidUser.match, match);
    }
}
