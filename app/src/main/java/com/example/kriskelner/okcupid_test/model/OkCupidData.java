package com.example.kriskelner.okcupid_test.model;


import java.util.List;

/**
 * Created by kris.kelner on 1/30/18.
 */

public class OkCupidData {
    public String total_matches;
    public List<OkCupidUser> data;

}
