package com.example.kriskelner.okcupid_test.util;

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kris.kelner on 1/30/18.
 */

public class ServiceApi {
    private String REST_ENDPOINT = "https://www.okcupid.com/";

    private OkCupidService okCupidApi;

    private Retrofit retrofit;

    private void initRetrofit() {
        if(retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(REST_ENDPOINT)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }

    public OkCupidService getApi(){
        if(okCupidApi==null){
            initRetrofit();
            okCupidApi = retrofit.create(OkCupidService.class);
        }
        return okCupidApi;
    }
}
