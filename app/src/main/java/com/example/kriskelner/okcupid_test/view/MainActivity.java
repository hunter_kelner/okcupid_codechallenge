package com.example.kriskelner.okcupid_test.view;

import android.graphics.Rect;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TabHost;

import com.example.kriskelner.okcupid_test.R;
import com.example.kriskelner.okcupid_test.adapter.DataAdapter;
import com.example.kriskelner.okcupid_test.model.OkCupidUser;
import com.example.kriskelner.okcupid_test.model.ViewModel;
import com.example.kriskelner.okcupid_test.presenter.MainPresenter;
import com.example.kriskelner.okcupid_test.util.OnItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


public class MainActivity extends AppCompatActivity{

    @BindView(R.id.rvUsers)
    RecyclerView rvUsers;
    @BindView(android.R.id.tabhost)
    TabHost tabHost;

    private DataAdapter rvAdapter;

    private Disposable viewModelSubscription;
    private MainPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setElevation(0);
            actionBar.setTitle(R.string.main_title);
        }
        presenter = new MainPresenter(this);
        viewModelSubscription = presenter.getViewModel(new Consumer<ViewModel>() {
            @Override
            public void accept(ViewModel viewModel) throws Exception {
                displayUsers(viewModel);
            }
        });
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        rvUsers.addItemDecoration(new SpacesItemDecoration(30));
        rvUsers.setLayoutManager(manager);
        rvUsers.setHasFixedSize(true);
        tabHost.setup();
        TabHost.TabSpec tab1 = tabHost.newTabSpec(getString(R.string.tab1_id));
        TabHost.TabSpec tab2 = tabHost.newTabSpec(getString(R.string.tab2_id));
        TabHost.TabContentFactory factory = new TabHost.TabContentFactory() {
            @Override
            public View createTabContent(String s) {
                return rvUsers;
            }
        };
        tab1.setIndicator(getString(R.string.tab1_name));
        tab1.setContent(factory);
        tab2.setIndicator(getString(R.string.tab2_name));
        tab2.setContent(factory);
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                if(s.equals(getString(R.string.tab1_id))){
                    presenter.displayAllUsers();
                }else{
                    presenter.displayMatches();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModelSubscription.dispose();
    }

    private void displayUsers(ViewModel viewModel){
        if(rvAdapter==null) {
            rvAdapter = new DataAdapter(this, viewModel.data.data, new OnItemClickListener() {
                @Override
                public void onItemClick(OkCupidUser item) {
                    item.liked = !item.liked;
                    presenter.toggleLiked(item);
                }
            });
            rvUsers.setAdapter(rvAdapter);
        }else{
            rvAdapter.changeDataSet(viewModel.data.data);
        }

    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }
}
