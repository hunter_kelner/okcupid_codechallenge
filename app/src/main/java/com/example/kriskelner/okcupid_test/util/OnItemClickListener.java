package com.example.kriskelner.okcupid_test.util;

import com.example.kriskelner.okcupid_test.model.OkCupidUser;

/**
 * Created by kris.kelner on 2/12/18.
 */

public interface OnItemClickListener {
    void onItemClick(OkCupidUser item);
}